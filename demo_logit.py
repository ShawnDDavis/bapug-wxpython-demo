#!/bin/python
"""
    demo of controls to push button, add text to list.
    placement of controls is manual.
"""

import wx
import time

class MainFrame(wx.Frame):
    """
    A Frame that lets you enter text, press a button, and write it out to a list
    """

    def __init__(self, *args, **kw):
        # ensure the parent's __init__ is called
        super(MainFrame, self).__init__(*args, **kw)


        # and a status bar
        self.CreateStatusBar()
        self.SetStatusText("Welcome to wxPython!")

        # create a panel for the background
        panelBack = wx.Panel(self)

        # Base font to use on everything
        baseFont = wx.Font(wx.FontInfo(22).FaceName("Helvetica"))

        # Label for text box
        lblEnterText = wx.StaticText(panelBack, label="Enter some text:", pos=(20, 20), size=(140, 40))
        lblEnterText.SetFont(baseFont)


        # text box where they can enter data
        self.txtEnteredText = wx.TextCtrl(panelBack, value="Enter some text here", pos=(240, 20),  size=(380, 40))
        self.txtEnteredText.SetFont(baseFont)

        # button which should cause text to be written to list
        self.btnEnter = wx.Button(panelBack, label='Log It!', pos=(630, 20), size=(90, 40))
        self.btnEnter.SetFont(baseFont)
        self.btnEnter.Bind(wx.EVT_BUTTON, self.OnLogIt)

        # Add the text to display entries
        self.textLog = wx.TextCtrl(panelBack, -1, pos=(20, 80), size=(700,400), style=wx.TE_MULTILINE|wx.TE_READONLY)
        self.textLog.SetFont(baseFont)

        self.Size = (760, 560)


    def OnLogIt(self, event):
        """
        Write the text from the textbox to the list
        """
        self.textLog.AppendText(time.strftime("%Y-%m-%d %H:%M:%S") + "  " + self.txtEnteredText.GetValue() + "\n")
        self.SetStatusText(self.txtEnteredText.GetValue())



if __name__ == '__main__':
    # When this module is run (not imported) then create the app, the
    # frame, show it, and start the event loop.
    app = wx.App()
    frm = MainFrame(None, title='Log It!')
    frm.Show()
    app.MainLoop()
