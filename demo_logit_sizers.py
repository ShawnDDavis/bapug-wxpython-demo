#!/bin/python
"""
demo of controls to push button, add text to list.
placement of controls uses wx Sizer.
"""

import wx
import time

class MainFrame(wx.Frame):
    """
    A Frame that lets you enter text, press a button, and write it out to a list
    """

    def __init__(self, *args, **kw):
        # ensure the parent's __init__ is called
        super(MainFrame, self).__init__(*args, **kw)


        # and a status bar
        self.CreateStatusBar()
        self.SetStatusText("Welcome to wxPython!")


        # Base font to use on all controls
        baseFont = wx.Font(wx.FontInfo(22).FaceName("Helvetica"))

        # Label for text box
        lblEnterText = wx.StaticText(self, label="Enter some text:")
        lblEnterText.SetFont(baseFont)


        # text box where they can enter data
        self.txtEnteredText = wx.TextCtrl(self, value="Enter some text here")
        self.txtEnteredText.SetFont(baseFont)

        # button which should cause text to be written to list
        self.btnEnter = wx.Button(self, label='Log It!')
        self.btnEnter.SetFont(baseFont)
        self.btnEnter.Bind(wx.EVT_BUTTON, self.OnLogIt)

        # Add the text to display entries
        self.textLog = wx.TextCtrl(self, -1,style = wx.TE_MULTILINE|wx.TE_READONLY)
        self.textLog.SetFont(baseFont)

        self.Size = (760, 560)

        # Setup the GridBag Sizer to manage position and size of the controls
        self.gbs = wx.GridBagSizer(vgap=5, hgap=5)

        self.gbs.Add(lblEnterText,  pos=(0, 0), flag=wx.TOP | wx.LEFT | wx.ALIGN_CENTER_VERTICAL, border=5, span=(1, 1))
        self.gbs.Add(self.txtEnteredText,pos=(0, 1), flag=wx.EXPAND | wx.TOP, border=5, span=(1, 1))
        self.gbs.Add(self.btnEnter, pos=(0, 2), flag=wx.TOP | wx.RIGHT, border=5, span=(1, 1))
        self.gbs.Add(self.textLog, pos=(1, 0), flag=wx.EXPAND | wx.ALL, border=5, span=(1, 3))

        self.gbs.AddGrowableRow(1, 2)
        self.gbs.AddGrowableCol(1, 2)
        self.SetSizer(self.gbs)
        self.gbs.Layout()
        self.Layout()


    def OnLogIt(self, event):
        """
        Write the text from the textbox to the list
        """
        self.textLog.AppendText(time.strftime("%Y-%m-%d %H:%M:%S") + "  " + self.txtEnteredText.GetValue() + "\n")
        self.SetStatusText(self.txtEnteredText.GetValue())



if __name__ == '__main__':
    # When this module is run (not imported) then create the app, the
    # frame, show it, and start the event loop.
    app = wx.App()
    frm = MainFrame(None, title='Log It! (with Sizer)')
    frm.Show()
    app.MainLoop()
